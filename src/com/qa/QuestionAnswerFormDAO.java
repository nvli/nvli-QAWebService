package com.qa;

import java.util.HashMap;
import java.util.List;

public interface QuestionAnswerFormDAO {
	
	public QuestionAnswerForm getAnswers(QuestionAnswerForm qstr);
	
}