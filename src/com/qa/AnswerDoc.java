package com.qa;
import java.util.ArrayList;

public class AnswerDoc{

String TaggedText, OrgText, doctitle, query;
ArrayList<String> ne_date, ne_location, ne_organization, ne_person, phrases;

	public String getTaggedText() {
		return TaggedText;
	}
	public void setTaggedText(String taggedText) {
		TaggedText = taggedText;
	}
	
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}

	public String getOrgText() {
		return OrgText;
	}
	public void setOrgText(String orgText) {
		OrgText = orgText;
	}
	public String getDoctitle() {
		return doctitle;
	}
	public void setDoctitle(String doctitle) {
		this.doctitle = doctitle;
	}
	public ArrayList<String> getNe_date() {
		return ne_date;
	}
	public void setNe_date(ArrayList<String> ne_date) {
		this.ne_date = ne_date;
	}
	public ArrayList<String> getNe_location() {
		return ne_location;
	}
	public void setNe_location(ArrayList<String> ne_location) {
		this.ne_location = ne_location;
	}
	public ArrayList<String> getNe_organization() {
		return ne_organization;
	}
	public void setNe_organization(ArrayList<String> ne_organization) {
		this.ne_organization = ne_organization;
	}
	public ArrayList<String> getNe_person() {
		return ne_person;
	}
	public void setNe_person(ArrayList<String> ne_person) {
		this.ne_person = ne_person;
	}
	public ArrayList<String> getPhrases() {
		return phrases;
	}
	public void setPhrases(ArrayList<String> phrases) {
		this.phrases = phrases;
	}
	public AnswerDoc(String taggedText, String orgText, String doctitle, String query, ArrayList<String> ne_date,
			ArrayList<String> ne_location, ArrayList<String> ne_organization, ArrayList<String> ne_person,
			ArrayList<String> phrases) {
		super();
		TaggedText = taggedText;
		OrgText = orgText;
		this.query = query;
		this.doctitle = doctitle;
		this.ne_date = ne_date;
		this.ne_location = ne_location;
		this.ne_organization = ne_organization;
		this.ne_person = ne_person;
		this.phrases = phrases;
	}

@Override
	public String toString() {
		return "AnswerDoc [TaggedText=" + TaggedText + ", OrgText=" + OrgText + ", doctitle=" + doctitle + ", ne_date="
				+ ne_date + ", ne_location=" + ne_location + ", ne_organization=" + ne_organization + ", ne_person="
				+ ne_person + ", phrases=" + phrases + "]";
	}
} 
