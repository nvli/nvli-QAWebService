package com.qa;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path; 
import javax.ws.rs.Produces; 
import javax.ws.rs.core.MediaType;

@Path("/QASys") 

public class QAService {  
   //UserDao userDao = new UserDao();  
   
   /*@GET 
   @Path("/users") 
   @Produces(MediaType.APPLICATION_JSON) 
   public List<User> getUsers(){ 
      return userDao.getAllUsers(); 
   }*/
   
   
   @POST
   @Path("/getanswer")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces(MediaType.APPLICATION_JSON) 
   public QuestionAnswerForm getTaggedText(QuestionAnswerForm us){
	   String ques = us.getQuestionString();
	   Solrjtest bp = new Solrjtest();
	   HashMap<String,List<String>> re = bp.getAnswers(ques, "http://sp-hadoop5:8999/solr/qa_test");
	   us.setAnswers(re);
	   return us; 
   }
   
}