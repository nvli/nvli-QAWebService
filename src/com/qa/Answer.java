package com.qa;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Answer {
	
	private boolean isEM;
	private boolean isRm;
	private boolean isSM;
	private boolean isSWP;
	private double pOTM;
	private String answer;
	private String plainTextAnswer;
	private double boost;
	private double rMB;
	private boolean isNeFound;
	private String note;
	private HashMap<String, ArrayList<String>> neFoundList;
	
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public boolean isNeFound() {
		return isNeFound;
	}
	public void setNeFound(boolean isNeFound) {
		this.isNeFound = isNeFound;
	}
	public boolean isEM() {
		return isEM;
	}
	public void setEM(boolean isEM) {
		this.isEM = isEM;
	}
	public boolean isSM() {
		return isSM;
	}
	public void setSM(boolean isSM) {
		this.isSM = isSM;
	}
	public double getpOTM() {
		return pOTM;
	}
	public void setpOTM(double pOTM) {
		this.pOTM = pOTM;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public double getBoost() {
		return boost;
	}
	public void setBoost(double boost) {
		this.boost = boost;
	}
	public String getPlainTextAnswer() {
		if(neFoundList!=null && neFoundList.size()>0){
			for ( Map.Entry<String, ArrayList<String>> entry : neFoundList.entrySet()) {
			    //String key = entry.getKey();
			    ArrayList<String> neArr = entry.getValue();
			    for(String keyword : neArr){
			    	plainTextAnswer = " "+plainTextAnswer+" ";
			    	plainTextAnswer = plainTextAnswer.replaceAll(" (?i)"+keyword.replaceAll("_", " ")+" ", "<span style=\"color:red\"> "+keyword.replaceAll("_", " ")+" </span>");
			    }
			    
			}
			
		}
		return plainTextAnswer;
	}
	public void setPlainTextAnswer(String plainTextAnswer) {
		this.plainTextAnswer = plainTextAnswer;
	}
	public HashMap<String, ArrayList<String>> getNeFoundList() {
		return neFoundList;
	}
	public void setNeFoundList(HashMap<String, ArrayList<String>> neFoundList) {
		this.neFoundList = neFoundList;
	}
	public boolean isSWP() {
		return isSWP;
	}
	public void isSWP(boolean isSWP) {
		this.isSWP = isSWP;
	}
	public boolean isRm() {
		return isRm;
	}
	public void setReducedmatch(boolean isRm) {
		this.isRm = isRm;
	}
	public double getrMB() {
		return rMB;
	}
	public void setrMB(double rMB) {
		this.rMB = rMB;
	}
	/**
	 * Criteria:
	 * isEM = 1
	 * isSM = 1
	 * isSWP = 1
	 * pOTM = 1
	 * isNeFound = 1
	 * @return
	 */
	public double calculateBoost(){
		double totalBoost = 0;
		
		if(isEM()){
			totalBoost++;
		}
		
		if(isNeFound()){
			totalBoost++;
		}
		
		if(!isSWP()){
			totalBoost++;
		}
		
		if(isSM()){
			totalBoost++;
		}
		
		if(isRm){
			totalBoost += rMB;
		}
		
		totalBoost += pOTM/100;	
		
		return totalBoost;
	}
}
