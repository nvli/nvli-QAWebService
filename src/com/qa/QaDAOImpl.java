package com.qa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class QaDAOImpl implements QuestionAnswerFormDAO {
	
	HashMap<String, List<String>> answers;
	Solrjtest hitsolr = new Solrjtest();
	QuestionAnswerForm result;
	
	public QuestionAnswerForm getAnswers(QuestionAnswerForm qstr)
	{
		answers = hitsolr.getAnswers(qstr.getQuestionString(),"http://sp-hadoop5:8999/solr/qa_test");
		result.setAnswers(answers);
		return result;
	}
}
